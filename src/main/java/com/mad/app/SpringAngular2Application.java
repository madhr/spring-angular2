package com.mad.app;

import com.mad.app.domain.Book;
import com.mad.app.service.BookService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@SpringBootApplication
public class SpringAngular2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringAngular2Application.class, args);
	}

	@Bean
	CommandLineRunner runner(BookService bookService){
		return args -> {
			bookService.save(new Book(1L,"Crime and punishment","Fyodor Dostoyevsky", "One of the most important books for me.", LocalDate.now().plus(5, ChronoUnit.DAYS), LocalDate.now().plus(-10, ChronoUnit.DAYS), false));
			bookService.save(new Book(2L,"1984","George Orwell", "Everyone should read this.", LocalDate.now().plus(10, ChronoUnit.DAYS), LocalDate.now().plus(-7, ChronoUnit.DAYS), false));
			bookService.save(new Book(3L,"For whom the bell tolls","Ernest Hemingway", "That one is good.", LocalDate.now().plus(10, ChronoUnit.DAYS), LocalDate.now().plus(-6, ChronoUnit.DAYS), false));
			bookService.save(new Book(4L,"Catcher in the rye","J.D. Salinger", "Very important for teenagers.", LocalDate.now().plus(10, ChronoUnit.DAYS), LocalDate.now().plus(-16, ChronoUnit.DAYS), false));
			bookService.save(new Book(5L,"The Emperor","Ryszard Kapuscinski", "Probably one of the most important works ever.", LocalDate.now().plus(10, ChronoUnit.DAYS), LocalDate.now().plus(-3, ChronoUnit.DAYS), false));
		};
	}
}
