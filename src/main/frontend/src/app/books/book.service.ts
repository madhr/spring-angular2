import {Http, Response} from "@angular/http";
import "rxjs/Rx";
import {Book} from "./book.model";
import {EventEmitter, Injectable} from "@angular/core";

@Injectable()
export class BookService {

  onBookAdded = new EventEmitter<Book>();

  constructor(private http: Http){}

  getBooks(){
    return this.http.get('/api/books')
      .map(
        (response: Response) => {
          return response.json();
        }
      );
  }

  getBookById(id: number){
    return this.http.get('/api/books/'+id)
      .map(
        (response: Response) => {
          return response.json();
        }
      );
  }

  addBook(book: Book){
    return this.http.post('api/books/save', book)
      .map(
        (response: Response) => {
          return response.json();
        }
    );
  }

  saveBook(book: Book, checked: boolean) {
    book.read = checked;
    return this.http.post('/api/books/save', book)
      .map(
        (response: Response) => {
          return response.json();
        }
      )
  }

  deleteBook(book: Book){
    return this.http.delete('/api/books/delete/'+book.id);
  }
}
