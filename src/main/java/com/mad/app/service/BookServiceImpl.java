package com.mad.app.service;

import com.mad.app.domain.Book;
import com.mad.app.repository.BookRepository;
import org.springframework.stereotype.Service;

/**
 * Created by Magda on 01.08.2017.
 */
@Service
public class BookServiceImpl implements BookService {

    private BookRepository bookRepository;

    public BookServiceImpl(BookRepository bookRepository){
        this.bookRepository = bookRepository;
    }

    @Override
    public Iterable<Book> list(){
        return bookRepository.findAll();
    }

    @Override
    public Book save(Book book){
        return bookRepository.save(book);
    }

    @Override
    public void delete(Long id) { bookRepository.delete(id); }

    @Override
    public Book getBookById(Long id) { return bookRepository.findOne(id); }
}
