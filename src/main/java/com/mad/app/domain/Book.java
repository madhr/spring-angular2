package com.mad.app.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDate;

/**
 * Created by Magda on 01.08.2017.
 */
@Entity
@Data
@AllArgsConstructor
public class Book {

    @Id
    @GeneratedValue
    private Long id;
    private String title;
    private String author;
    private String notes;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate dueDate;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate dateAdded;
    private boolean read;

    public Book() {}
}
