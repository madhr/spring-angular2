export class Book {
  public id: number;
  public author: string;
  public title: string;
  public notes: string;
  public dueDate: string;
  public dateAdded: string;
  public read: boolean;

  constructor(author: string, title: string, notes: string, dueDate: string, dateAdded: string, read: boolean){
    this.author = author;
    this.title = title;
    this.notes = notes;
    this.dueDate = dueDate;
    this.dateAdded = dateAdded;
    this.read = read;
  }
}
