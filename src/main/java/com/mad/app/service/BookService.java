package com.mad.app.service;

import com.mad.app.domain.Book;

/**
 * Created by Magda on 01.08.2017.
 */
public interface BookService {

    Iterable<Book> list();

    Book save(Book book);

    void delete(Long id);

    Book getBookById(Long id);
}
