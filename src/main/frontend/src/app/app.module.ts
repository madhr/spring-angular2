import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { BookComponent } from './books/books.component';
import { BookService } from './books/book.service';
import { BooksListComponent } from './books/books-list/books-list.component';
import { BooksAddComponent } from './books/books-add/books-add.component';

@NgModule({
  declarations: [
    AppComponent,
    BookComponent,
    BooksListComponent,
    BooksAddComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    DatepickerModule.forRoot(),
    HttpModule
  ],
  providers: [BookService],
  bootstrap: [AppComponent]
})
export class AppModule { }
