package com.mad.app.controller;

import com.mad.app.domain.Book;
import com.mad.app.service.BookService;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Magda on 01.08.2017.
 */
@RestController
@RequestMapping("/api/books")
public class BookController {

    private BookService bookService;

    public BookController(BookService bookService){
        this.bookService = bookService;
    }

    @GetMapping(value = {"", "/"})
    public Iterable<Book> listBooks(){
        return bookService.list();
    }

    @PostMapping("/save")
    public Book saveBook(@RequestBody Book book){
        return bookService.save(book);
    }

    @RequestMapping(value = "/{id}")
    public Book getBookById(@PathVariable Long id){
        return bookService.getBookById(id);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public void deleteBook(@PathVariable Long id){
        bookService.delete(id);
    }
}
