package com.mad.app.repository;

import com.mad.app.domain.Book;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Magda on 01.08.2017.
 */
@Repository
public interface BookRepository extends CrudRepository<Book,Long> {
}
