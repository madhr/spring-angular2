import { Component, OnInit } from '@angular/core';
import {Response} from "@angular/http";

import {Book} from "../book.model";
import {BookService} from "../book.service";

@Component({
  selector: 'app-books-list',
  templateUrl: './books-list.component.html',
  styleUrls: ['./books-list.component.css']
})
export class BooksListComponent implements OnInit {

  books: Book[] = [];

  constructor(private bookService: BookService) {

  }

  ngOnInit() {
    // initial load of data
    this.bookService.getBooks()
      .subscribe(
        (books: any[]) => {
          this.books = books
        },
        (error) => console.log(error)
      );
    // get notified when a new task has been added
    this.bookService.onBookAdded.subscribe(
      (book: Book) => this.books.push(book)
    );
  }

  getDueDateLabel(book: Book){
    return book.read ? 'label-success' : 'label-primary';
  }

  onBookChange(event, book) {
    this.bookService.saveBook(book,event.target.checked).subscribe();
  }

  onDelete(book: Book){
    this.bookService.deleteBook(book).subscribe();
    this.bookService.getBooks()
      .subscribe(
        (books: any[]) => {
          this.books = books
        },
        (error) => console.log(error)
      );
  }

}
