import { Component, OnInit } from '@angular/core';
import {BookService} from "../book.service";
import {Book} from "../book.model";

@Component({
  selector: 'app-books-add',
  templateUrl: './books-add.component.html',
  styleUrls: ['./books-add.component.css']
})
export class BooksAddComponent implements OnInit {

  constructor(private bookService: BookService) { }

  public book: Book;

  ngOnInit() {
    this.book = <Book>{
      author: '',
      title: '',
      notes: '',
      dueDate: '',
      dateAdded: '',
      read: false
    };
  }

  submitted = false;

  save(model: Book, isValid: boolean) {
    this.submitted = true;
    let book: Book = new Book(model.author,
      model.title,
      model.notes,
      this.getDateAsString(this.parseDate(model.dueDate)),
      this.getDateAsString(new Date()),
      false);
    this.bookService.addBook(book)
      .subscribe(
        (newBook: Book) => {
          this.bookService.onBookAdded.emit(newBook);
        }
      );
  }

  parseDate(dateString: string): Date {
    if (dateString) { return new Date(dateString); }
    else { return null; }
  }

  getDateAsString(date: Date) {
    let dd: any = date.getDate();
    let mm: any = date.getMonth() + 1;
    let yyyy = date.getFullYear();
    if (dd < 10) { dd = '0' + dd; }
    if (mm < 10) { mm = '0' + mm; }
    return dd + '/' + mm + '/' + yyyy;
  }

}
